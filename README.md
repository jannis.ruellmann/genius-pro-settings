# Welcome to genius-pro-settings

This project contains all the Setttings for my 3d printers.
Please contact me if you have any problems or have questions regarding this project. 
Feature Requests can be issued via: 
incoming+jannis-ruellmann-genius-pro-settings-32872952-9dj6llkw8gs4p9lqwd81dn1s8-issue@incoming.gitlab.com
No worries they will reach me. :)


## Prerequisites

Install the newest version of Cura[https://ultimaker.com/de/software/ultimaker-cura]. 

Add a new (non) network printer. 

For the genius pro printer you can simply add the genius printer. 
After you can import the profiles that are stored in this repository.
Fell free to change them according to your needs.

Enjoy and happy Printing!
